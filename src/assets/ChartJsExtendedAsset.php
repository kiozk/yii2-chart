<?php
namespace kiozk\chart\assets;

use yii\web\AssetBundle;

class ChartJsExtendedAsset extends AssetBundle {
    public $sourcePath = '@vendor/kiozk/yii2-chart/assets/charjs-extended/';
    /**
     * @inheritdoc
     */
    public function init()
    {

        $this->js[] = YII_DEBUG ? 'extended-chart.js' : 'extended-chart.min.js';
        $this->css[] = YII_DEBUG ? 'style.css' : 'style.min.css';

        parent::init();
    }

    public $depends = [
        ChartJsAsset::class,
       // MomentAsset::class,
    ];
}
