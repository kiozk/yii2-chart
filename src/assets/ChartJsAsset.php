<?php
namespace kiozk\chart\assets;

use kiozk\datetime\assets\MomentAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class ChartJsAsset extends AssetBundle {
    public $sourcePath = '@vendor/nnnick/chartjs/dist';
    /**
     * @inheritdoc
     */
    public function init()
    {

        $this->js[] = YII_DEBUG ? 'Chart.js' : 'Chart.min.js';

        parent::init();
    }

    public $depends = [
        JqueryAsset::class,
        MomentAsset::class
    ];
}
