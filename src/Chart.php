<?php
namespace kiozk\chart;

use kiozk\chart\assets\ChartJsExtendedAsset;
use Yii;

use yii\base\InvalidArgumentException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;

/**
 * Class Chart
 * @package admin\chart
 *
 * @property DataSet[] $dataSets
 * @property Scale[] $scales
 * @property Scale[] $labels
 * @property Legend $legend
 */
class Chart extends Widget
{
    const COLORS = [
        '#7cb5ec',
        '#434348',
        '#90ed7d',
        '#f7a35c',
        '#8085e9',
        '#f15c80',
        '#e4d354',
        '#2b908f',
        '#f45b5b',
        '#91e8e1'
    ];

    //Типы графиков
    const TYPE_LINE             = 'line';
    const TYPE_DOUGHNUT         = 'doughnut';
    const TYPE_PIE              = 'pie';
    const TYPE_BAR              = 'bar';
    const TYPE_HORIZONTAL_BAR   = 'horizontalBar';

    //Наборы педустоновок для графика
    const PROFILE_STRING_DATE_X             = 'string_date_x';
    const PROFILE_LOGARITHMIC_Y             = 'logarithmic_y';
    const PROFILE_STRING_DATETIME_X         = 'string_datetime_x';
    const PROFILE_INT_DATETIME_HOURLY_X     = 'int_datetime_hourly_x';
    const PROFILE_REDUCTION_LINEAR_Y        = 'reduction_linear_y';
    const PROFILE_STACKED_WITH_PROPORTION_Y = 'staked_with_y_proportion';
    const PROFILE_PIE_SIZE_WITH_PROPORTION  = 'profile_pie_size_with_proportion';
    const PROFILE_PIE_WITH_PROPORTION       = 'profile_pie_with_proportion';
    const PROFILE_TIME_IN_MINUTES_Y         = 'profile_time_in_minutes_y';

    /**
     * @var array
     */

    public $options = [];
    /**
     * @var array
     */

    public $clientOptions = [];
    /**
     * @var DataSet[]
     */

    private $_dataSets;

    /**
     * @var Scale[]
     */
    private $_scales;

    /**
     *
     */
    private $_labels;

    /**
     * @var Tooltip|null
     */
    private $_tooltip;

    /**
     * @var string
     */
    public $type = Chart::TYPE_LINE;

    /**
     * @var bool
     */
    public $responsive = true;
    /**
     * @var string|string[]
     */
    public $profile;

    /**
     * @var Legend|null
     */
    public $_legend;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (is_array($this->profile)) {
            foreach ($this->profile as $profile) {
                $this->loadFromProfile($profile);
            }
        } else {
            $this->loadFromProfile($this->profile);
        }

    }

    /**
     * @param $profile
     * @throws \yii\base\InvalidConfigException
     */
    protected function loadFromProfile($profile)
    {
        if ($profile === static::PROFILE_STRING_DATE_X) {
            $this->addScale(
                [
                    'axe' => Scale::AXE_X,
                    'type' => 'stringDate',

                ]
            );

        } elseif ($profile === static::PROFILE_STACKED_WITH_PROPORTION_Y) {
            $this->addScale(
                [
                    'axe' => Scale::AXE_Y,
                    'type' => 'linearWithProportion',
                    'stacked' => true,
                    'ticks' => [
                        'beginAtZero' => true
                    ]
                ]
            );
            $dataSets = $this->getDataSets();
            foreach ($dataSets as $dataSet) {
                $dataSet->stack = 'ds';
            }
            $this->setTooltip([
                'mode' => 'index',
                'callbacks' => [
                    'footer' => new JsExpression(/**@lang JavaScript */
                        'function(tooltipItems, data) {
                                        var sum = 0;
            
                                        tooltipItems.forEach(function(tooltipItem) {
                                            sum += data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                        });
                                        return \'' . Yii::t('app', 'Total') . ': \' + sum;
                                    }'),
                ]
            ]);
        } elseif ($profile === static::PROFILE_REDUCTION_LINEAR_Y) {
            $this->addScale(
                [
                    'axe' => Scale::AXE_Y,
                    'type' => 'reductionLinear',
                    'stacked' => true
                ]
            );
        } elseif ($profile === static::PROFILE_TIME_IN_MINUTES_Y) {
            $this->addScale(
                [
                    'axe' => Scale::AXE_Y,
                    'type' => 'timeLinear',
                ]
            );
        } elseif ($profile === static::PROFILE_INT_DATETIME_HOURLY_X) {

            $this->addScale(
                [
                    'axe' => Scale::AXE_X,
                    'type' => 'timestampHourLinear',
                ]
            );
        } elseif ($profile === static::PROFILE_LOGARITHMIC_Y) {

            $this->addScale(
                [
                    'axe' => Scale::AXE_Y,
                    'type' => 'logarithmic',
                ]
            );
        } elseif ($profile === static::PROFILE_PIE_WITH_PROPORTION) {
            $this->setTooltip([

                'callbacks' => [
                    'label' => new JsExpression(/**@lang JavaScript */
                        "(function(tooltipItem, data) {
					var index =  tooltipItem.index, datasetIndex = tooltipItem.datasetIndex,
					dataLabel = data.labels[index], 
					sum = 0, 
					value =  data.datasets[datasetIndex].data[index], percentage;
					
				    $.each(data.datasets[datasetIndex].data, function () {
                        sum +=  this;
                    });

				     if(sum > 0){
                        percentage = value / sum;
                    } else {
                        percentage = 0.0;
                    }
					
	
					return dataLabel + ': ' + value  + ' (' + Chart.Ticks.formatters.percentage(percentage) + ')';
				})")
                ]]);
        } elseif ($profile === static::PROFILE_PIE_SIZE_WITH_PROPORTION) {
            $this->setTooltip(['callbacks' => [
                'label' => new JsExpression(/**@lang JavaScript */
                    "(function(tooltipItem, data) {
					var index =  tooltipItem.index, datasetIndex = tooltipItem.datasetIndex,
					dataLabel = data.labels[index], 
					sum = 0, 
					value =  data.datasets[datasetIndex].data[index], percentage;
					
				    $.each(data.datasets[datasetIndex].data, function () {
                        sum +=  this;
                    });

				     if(sum > 0){
                        percentage = value / sum;
                    } else {
                        percentage = 0.0;
                    }
					
	
					return dataLabel + ': ' + Chart.Ticks.formatters.size(value)  + ' (' + Chart.Ticks.formatters.percentage(percentage) + ')';
				})")
            ]]);
            /* $this->addScale(
                 [
                     'axe'   => Scale::AXE_Y,
                     'type'  => 'category',
                 ]
             );*/
        }
    }

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $id = $this->getId();

        $this->registerAssets();
        return Html::beginTag('div', ['class' => 'chart', 'id' => $id]) .
            Html::tag('canvas', '', ArrayHelper::merge($this->options, ['id' => $id . '-canvas'])) .
            Html::endTag('div');
    }

    /**
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function registerAssets()
    {
        $view = $this->getView();
        $id = $this->getId();

        $typeStr = (in_array($this->type, [
                static::TYPE_DOUGHNUT,
                static::TYPE_LINE,
                static::TYPE_PIE,
                static::TYPE_BAR,
                static::TYPE_HORIZONTAL_BAR]
        )) ? $this->type : static::TYPE_LINE;

        $params = [
            'type' => $typeStr,
            'data' => $this->prepareData(),
        ];

        $scales = $this->getScales();
        if (is_array($scales) && count($scales) > 0) {
            $params['options']['scales'] = $this->prepareScales();
        }

        if ((null !== $tooltip = $this->_tooltip) && null !== $prepared = $tooltip->prepare()) {
            $params['options']['tooltips'] = $prepared;
        }

        if ((null !== $legend = $this->_legend) && null !== $prepared = $legend->prepare()) {
            $params['options']['legend'] = $prepared;
        }

        $params['options']['onResize'] = new JsExpression(/** @var  */"function(e, a, b){console.dir(e);console.dir(a);console.dir(b);}");
        $params['options']['maintainAspectRatio'] = false;
        $tHash = base_convert(md5($id), 16, 36);
        $paramsAsJson = Json::encode($params);

        ChartJsExtendedAsset::register($this->view);

        $lang = Yii::$app->language;
        $js = /** @lang JavaScript */
            "
try {
  //  moment.locale('{$lang}');
    var canvas{$tHash} = $('#{$id}-canvas');
            if(canvas{$tHash}.length){
                var chart{$tHash} = new Chart(canvas{$tHash}.get(0).getContext('2d'), {$paramsAsJson});
                canvas{$tHash}.data('chart', chart{$tHash});
            }
}catch (e){
    console.warn(e);
}";
        $view->registerJs($js);
        // $view->registerJs(YII_DEBUG ? $js : JSPacker::encode($js));

    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function prepareScales()
    {
        $result = [];
        $scales = $this->getScales();
        foreach ($scales as $scale) {
            if ($scale->getIsAxeX()) {
                $result['xAxes'][] = $scale->prepare();
            } elseif ($scale->getIsAxeY()) {
                $result['yAxes'][] = $scale->prepare();
            }
        }

        return $result;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function prepareData()
    {
        return [
            'labels' => $this->prepareLabels(),
            'datasets' => $this->prepareDataSets(),
        ];
    }

    /**
     * @return array|string[]
     */
    protected function prepareLabels()
    {
        $labels = $this->getLabels();

        return array_values($labels);
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    protected function prepareDataSets()
    {
        $result = [];
        $dataSets = $this->getDataSets();

        foreach ($dataSets as $dataSet) {
            $result[] = $dataSet->prepare();
        }
        return $result;
    }

    /**
     * @param array|DataSet[] $dataSets
     * @throws \yii\base\InvalidConfigException
     */
    public function setDataSets($dataSets = [])
    {

        $tmpDataSet = [];
        foreach ($dataSets as $dataSet) {
            if (is_array($dataSet)) {
                $dataSetConfig = [
                    'class' => DataSet::class,
                    'data' => $dataSet
                ];

                $dataSet = Yii::createObject($dataSetConfig);

            } elseif (!$dataSet instanceof DataSet) {
                throw new InvalidArgumentException('Only DataSet instance, configuration array or false is allowed.');
            }

            $tmpDataSet[] = $dataSet;
        }

        $this->_dataSets = $tmpDataSet;
    }

    /**
     * @return DataSet[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getDataSets()
    {
        if ($this->_dataSets === null) {
            $this->setDataSets([]);
        }
        return $this->_dataSets;
    }

    /**
     * @param array|Scale[] $scales
     * @throws \yii\base\InvalidConfigException
     */
    public function setScales($scales = [])
    {

        $tmpScales = [];
        foreach ($scales as $scale) {
            if (is_array($scale)) {
                $scale = Yii::createObject(array_merge(['class' => Scale::class], $scale));
            } elseif (!$scale instanceof Scale) {
                throw new InvalidArgumentException('Only Scale instance, configuration array or false is allowed.');
            }

            $tmpScales[] = $scale;
        }

        $this->_scales = $tmpScales;
    }

    /**
     * @param Scale|array $scale
     * @throws \yii\base\InvalidConfigException
     */
    protected function addScale($scale)
    {
        $scales = $this->getScales();
        if (is_array($scale)) {
            $scale = Yii::createObject(array_merge(['class' => Scale::class], $scale));
        } elseif (!$scale instanceof Scale) {
            throw new InvalidArgumentException('Only Scale instance, configuration array or false is allowed.');
        }

        $scales[] = $scale;

        $this->setScales($scales);
    }

    /**
     * @return Scale[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getScales()
    {
        if ($this->_scales === null) {
            $this->setScales([]);
        }
        return $this->_scales;
    }

    /**
     * @param int $index
     *
     * @return string
     */
    public static function colorByIndex($index)
    {
        $c = count(static::COLORS);

        return static::COLORS[$index % $c];
    }

    protected static function rgbToRgba($rgb, $opacity)
    {
        list($r, $g, $b) = sscanf($rgb, "#%02x%02x%02x");
        return "rgba({$r}, {$g}, {$b}, {$opacity})";
    }

    /**
     * @return null
     */
    public function getLabels()
    {
        if ($this->_labels === null) {
            $this->setLabels([]);
        }

        return $this->_labels;
    }

    /**
     * @param $value
     */
    public function setLabels($value)
    {
        if (is_array($value)) {
            $this->_labels = $value;
        } else {
            throw new InvalidArgumentException('Only "labels" array allowed.');
        }
    }

    /**
     * @return Tooltip|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getTooltip()
    {
        if ($this->_tooltip === null) {
            $this->setTooltip([]);
        }

        return $this->_tooltip;
    }

    /**
     * @param Tooltip|array $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setTooltip($value)
    {
        if (is_array($value)) {
            $this->_tooltip = Yii::createObject(array_merge(['class' => Tooltip::class], $value));
        } elseif ($value instanceof Tooltip) {
            $this->_tooltip = $value;
        } else {
            throw new InvalidArgumentException('Only Tooltip instance, configuration array or false is allowed.');
        }
    }

    /**
     * @return Legend|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getLegend()
    {
        if ($this->_legend === null) {
            $this->setLegend([]);
        }

        return $this->_legend;
    }

    /**
     * @param Legend|array $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setLegend($value)
    {
        if (is_array($value)) {
            $this->_legend = Yii::createObject(array_merge(['class' => Legend::class], $value));
        } elseif ($value instanceof Legend) {
            $this->_legend = $value;
        } else {
            throw new InvalidArgumentException('Only Legend instance, configuration array or false is allowed.');
        }
    }
}