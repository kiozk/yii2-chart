<?php
namespace kiozk\chart;

use yii\web\JsExpression;

class TooltipCallbacks {
    /**
     * 	Array[tooltipItem], data	Returns the text to render before the title.
     * @var JsExpression|null
     */
    public $beforeTitle;

    /**
     * Array[tooltipItem], data	Returns text to render as the title of the tooltip.
     * @var JsExpression|null
     */
    public $title;

    /**
     * 	Array[tooltipItem], data	Returns text to render after the title.
     * @var JsExpression|null
     */
    public $afterTitle;

    /**
     * Arguments js function: Array[tooltipItem], data
     *
     * Returns text to render before the body section.
     *
     * @var JsExpression|null
     */
    public $beforeBody;

    /**
     * Arguments js function: tooltipItem, data
     *
     * Returns text to render before an individual label. This will be called for each item in the tooltip.
     *
     * @var JsExpression|null
     */
    public $beforeLabel;
    /**
     * Arguments js function:	tooltipItem, data	Returns text to render for an individual item in the tooltip.
     * @var JsExpression|null
     */
    public $label;

    /**
     * Arguments js function: tooltipItem, chart
     *
     * Returns the colors to render for the tooltip item. more...
     *
     * @var JsExpression|null
     */
    public $labelColor;

    /**
     * Arguments js function:tooltipItem, data
     *
     * Returns text to render after an individual label.
     *
     * @var JsExpression|null
     */
    public $afterLabel;

    /**
     * Arguments js function:Array[tooltipItem], data
     *
     * Returns text to render after the body section
     *
     * @var JsExpression|null
     */
    public $afterBody;

    /**
     * 	Arguments js function:Array[tooltipItem], data
     *
     * Returns text to render before the footer section.
     *
     * @var JsExpression|null
     */
    public $beforeFooter;

    /**
     * Arguments js function:Array[tooltipItem], data
     *
     * Returns text to render as the footer of the tooltip.
     *
     * @var JsExpression|null
     */
    public $footer;

    /**
     * Arguments js function: Array[tooltipItem], data
     *
     * Text to render after the footer section
     *
     * @var JsExpression|null
     */
    public $afterFooter;

    /**
     * @return array
     */
    public function prepare()
    {
        $result = [];

        if($this->beforeTitle !== null){
            $result['beforeTitle'] = $this->beforeTitle;
        }

        if($this->title !== null){
            $result['title'] = $this->title;
        }

        if($this->afterTitle !== null){
            $result['afterTitle'] = $this->afterTitle;
        }

        if($this->beforeBody !== null){
            $result['beforeBody'] = $this->beforeBody;
        }
        if($this->beforeBody !== null){
            $result['beforeBody'] = $this->beforeBody;
        }
        if($this->beforeLabel !== null){
            $result['beforeLabel'] = $this->beforeLabel;
        }

        if($this->label !== null){
            $result['label'] = $this->label;
        }

        if($this->labelColor !== null){
            $result['labelColor'] = $this->labelColor;
        }

        if($this->afterLabel !== null){
            $result['label'] = $this->afterLabel;
        }

        if($this->afterBody !== null){
            $result['afterBody'] = $this->afterBody;
        }

        if($this->beforeFooter !== null){
            $result['beforeFooter'] = $this->beforeFooter;
        }

        if($this->footer !== null){
            $result['footer'] = $this->footer;
        }

        if($this->afterFooter !== null){
            $result['afterFooter'] = $this->afterFooter;
        }

        if(count($result) === 0){
            return null;
        }
        return $result;
    }
}