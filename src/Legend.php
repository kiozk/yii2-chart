<?php
namespace kiozk\chart;

use yii\base\InvalidArgumentException;
use yii\base\BaseObject;
use yii\web\JsExpression;

/**
 * Class Legend
 * @package admin\chart
 *
 * @property string $position
 */
class Legend extends BaseObject{
    /**
     * Bottom side
     */
    const POSITION_BOTTOM   = 'bottom';

    /**
     * Left side
     */
    const POSITION_LEFT     = 'left';

    /**
     * Right side
     */
    const POSITION_RIGHT    = 'right';

    /**
     * Top side
     */
    const POSITION_TOP      = 'top';
    /**
     * @var bool|null Is the legend displayed
     *
     * Default: true
     */
    public $display;

    /**
     * @var string|null Position of the legend. Possible values are 'top', 'left', 'bottom' and 'right'.
     *
     * Default: top
     */
    private $_position;

    /**
     * @var bool|null Marks that this box should take the full width of the canvas (pushing down other boxes)
     *
     * Default: true
     */
    public $fullWidth;

    /**
     * @var JsExpression|null A callback that is called when a 'click' event is registered on top of a label item
     */
    public $onClick;

    /**
     * @var JsExpression|null A callback that is called when a 'mousemove' event is registered on top of a label item
     */
    public $onHover;

    /**
     * Legend will show datasets in reverse order
     *
     * Default: false
     * @var bool|null
     */
    public $reverse;

    public function prepare(){
        $result = [];

        if($this->display !== null){
            $result['display'] = (bool)$this->display;
        }

        if($this->_position !== null){

            $result['position'] = $this->_position;
        }

        if($this->fullWidth !== null){
            $result['fullWidth'] = (bool)$this->fullWidth;
        }

        if($this->onClick !== null){
            $result['onClick'] = $this->onClick;
        }

        if($this->onHover !== null){
            $result['onHover'] = $this->onHover;
        }

        if($this->reverse !== null){
            $result['reverse'] = (bool)$this->reverse;
        }
        return empty($result) ? null : $result;
    }

    /**
     * Position of the legend.
     * Possible values are 'top', 'left', 'bottom', 'right' and null.
     *
     * @param $value
     */
    public function setPosition($value){
        if($value === null){
            $this->_position = null;
        } elseif (is_string($value)){
            $value = strtolower($value);
            if(in_array($value, [static::POSITION_BOTTOM, static::POSITION_LEFT, static::POSITION_RIGHT, static::POSITION_TOP])){
                $this->_position = $value;
            } else {
                throw new InvalidArgumentException('Only "top", "right", "button", "left" string or null is allowed.');
            }
        } else {
            throw new InvalidArgumentException('Only "top", "right", "button", "left" string or null is allowed.');
        }
    }

    /**
     * Position of the legend.
     * Possible values are 'top', 'left', 'bottom', 'right' and null.
     *
     * @return null|string
     */
    public function getPosition(){
        return $this->_position;
    }
}