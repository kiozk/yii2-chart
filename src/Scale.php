<?php
namespace kiozk\chart;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\BaseObject;
use yii\web\JsExpression;

/**
 * Class Scale
 *
 * Multiple X & Y axes are supported.
 * A built-in label auto-skip feature detects would-be overlapping ticks and labels and removes every nth label to keep things displaying normally.
 * Scale titles are supported
 * New scale types can be extended without writing an entirely new chart type
 *
 * @package admin\chart
 *
 * @property GridLines $gridLines
 * @property ScaleLabel $scaleLabel
 * @property Ticks $ticks
 *
 * @property bool $isAxeX
 * @property bool $isAxeY
 *
 * @property string $string
 */
class Scale extends BaseObject {
    const TYPE_CATEGORY     = 'category';
    const TYPE_LINEAR       = 'linear';
    const TYPE_LOGARITHMIC  = 'logarithmic';
    const TYPE_TIME         = 'time';
    const TYPE_RADIALLINEAR = 'radialLinear';

    const AXE_X = 'x';
    const AXE_Y = 'y';

    /**
     * Bottom side
     */
    const POSITION_BOTTOM   = 'bottom';

    /**
     * Left side
     */
    const POSITION_LEFT     = 'left';

    /**
     * Right side
     */
    const POSITION_RIGHT    = 'right';

    /**
     * Top side
     */
    const POSITION_TOP      = 'top';

    /**
     * Type of scale being employed. Custom scales can be created and registered with a string key.
     *
     * Options: "category", "linear", "logarithmic", "time", "radialLinear"
     *
     * @var string|null
     */
    public $type;

    /**
     * If true, show the scale including gridlines, ticks, and labels. Overrides gridLines.display, scaleLabel.display, and ticks.display.
     *
     * Default: true
     *
     * @var bool|null
     */
    public $display;

    /**
     * Position of the scale. Possible values are 'top', 'left', 'bottom' and 'right'.
     *
     * @var string|null
     */
    private $_position;

    /**
     * @var string|null
     */
    public $id;

    /**
     * @var GridLines|null
     */
    protected $_gridLines;

    /**
     * @var ScaleLabel|null
     */
    protected $_scaleLabel;

    /**
     * @var Ticks|null
     */
    protected $_ticks;

    /**
     *
     * @var
     */
    protected $_axe;


    /**
     * If true, bars are stacked on the axis
     * @var bool
     */
    public $stacked;


    /**
     * Callback called before the update process starts. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeUpdate;

    /**
     * Callback that runs at the end of the update process. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterUpdate;

    /**
     * Callback that runs before dimensions are set. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeSetDimensions;

    /**
     * Callback that runs after dimensions are set. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterSetDimensions;

    /**
     * Callback that runs before data limits are determined. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeDataLimits;

    /**
     * Callback that runs after data limits are determined. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterDataLimits;

    /**
     * Callback that runs before ticks are created. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeBuildTicks;

    /**
     * Callback that runs after ticks are created. Useful for filtering ticks. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterBuildTicks;

    /**
     * Callback that runs before ticks are converted into strings. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeTickToLabelConversion;

    /**
     * Callback that runs after ticks are converted into strings. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterTickToLabelConversion;

    /**
     * Callback that runs before tick rotation is determined. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeCalculateTickRotation;

    /**
     * Callback that runs after tick rotation is determined. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterCalculateTickRotation;

    /**
     * Callback that runs before the scale fits to the canvas. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $beforeFit;

    /**
     * Callback that runs after the scale fits to the canvas. Passed a single argument, the scale instance.
     *
     * @var JsExpression|null
     */
    public $afterFit;


    /**
     * @var ScaleTime
     */
    private $_time;

    /**
     * If true, extra space is added to the both edges and the axis is scaled to fit into the chart area. This is set to true in the bar chart by default.
     *
     * @var null|false
     */
    public $offset;
    /**
     * @return ScaleLabel|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getScaleLabel(){
        if ($this->_scaleLabel === null) {
            $this->setScaleLabel([]);
        }

        return $this->_scaleLabel;
    }

    /**
     * @param $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setScaleLabel($value){
        if (is_array($value)) {
            $config = ['class' => GridLines::class];
            $this->_scaleLabel = Yii::createObject(array_merge($config, $value));
        } elseif ($value instanceof Ticks) {
            $this->_scaleLabel = $value;
        } else {
            throw new InvalidArgumentException('Only GridLines instance, configuration array or false is allowed.');
        }
    }

    /**
     * @return GridLines|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getGridLines(){
        if ($this->_gridLines === null) {
            $this->setGridLines([]);
        }

        return $this->_gridLines;
    }

    /**
     * @param $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setGridLines($value){
        if (is_array($value)) {
            $config = ['class' => GridLines::class];
            $this->_gridLines = Yii::createObject(array_merge($config, $value));
        } elseif ($value instanceof Ticks) {
            $this->_gridLines = $value;
        } else {
            throw new InvalidArgumentException('Only GridLines instance, configuration array or false is allowed.');
        }
    }

    /**
     * @return Ticks|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getTicks(){
        if ($this->_ticks === null) {
            $this->setTicks([]);
        }

        return $this->_ticks;
    }

    /**
     * @param Ticks|array $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setTicks($value){
        if (is_array($value)) {
            $this->_ticks = Yii::createObject(array_merge(['class' => Ticks::class], $value));
        } elseif ($value instanceof Ticks) {
            $this->_ticks = $value;
        } else {
            throw new InvalidArgumentException('Only Ticks instance, configuration array or false is allowed.');
        }
    }

    /**
     *
     */
    public function getAxe(){
        if($this->_axe === null) {
            $this->setAxe(static::AXE_X);
        }
        return $this->_axe;
    }

    /**
     * @param string $value
     */
    public function setAxe($value){
        $this->_axe = $value;
    }


    /**
     * @return bool
     */
    public function getIsAxeY(){
        return $this->getAxe() === static::AXE_Y;
    }

    /**
     * @return bool
     */
    public function getIsAxeX(){
        return $this->getAxe() === static::AXE_X;
    }

    /**
     * Position of the legend.
     * Possible values are 'top', 'left', 'bottom', 'right' and null.
     *
     * @param $value
     */
    public function setPosition($value){
        if($value === null){
            $this->_position = null;
        } elseif (is_string($value)){
            $value = strtolower($value);
            if(in_array($value, [static::POSITION_BOTTOM, static::POSITION_LEFT, static::POSITION_RIGHT, static::POSITION_TOP])){
                $this->_position = $value;
            } else {
                throw new InvalidArgumentException('Only "top", "right", "button", "left" string or null is allowed.');
            }
        } else {
            throw new InvalidArgumentException('Only "top", "right", "button", "left" string or null is allowed.');
        }
    }

    /**
     * Position of the legend.
     * Possible values are 'top', 'left', 'bottom', 'right' and null.
     *
     * @return null|string
     */
    public function getPosition(){
        return $this->_position;
    }

    /**
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function prepare(){
        $result = [];

        $gridLines      = $this->getGridLines();
        $ticks          = $this->getTicks();
        $scaleLabel     = $this->getScaleLabel();
        $time     = $this->getTime();

        if(null !== $prepared = $gridLines->prepare()){
            $result['gridLines'] = $prepared;
        }

        if(null !== $prepared = $ticks->prepare()){
            $result['ticks'] = $prepared;
        }


        if(null !== $prepared = $scaleLabel->prepare()){
            $result['scaleLabel'] = $prepared;
        }
        if(null !== $prepared = $time->prepare()){
            $result['time'] = $prepared;
        }

        /**
         * Base settings
         */
        if($this->id !== null){
            $result['id'] = $this->id;
        }
        if($this->type !== null){
            $result['type'] = $this->type;
        }

        if($this->display !== null){
            $result['display'] = (bool)$this->display;
        }
        if($this->_position !== null){
            $result['position'] = $this->_position;
        }

        if($this->stacked !== null){
            $result['stacked'] = (bool)$this->stacked;
        }
        if($this->offset !== null){
            $result['offset'] = (bool)$this->offset;
        }
        /**
         * Events section
         */
        if($this->beforeUpdate !== null){
            $result['beforeUpdate'] = $this->beforeUpdate;
        }
        if($this->afterUpdate !== null){
            $result['afterUpdate'] = $this->afterUpdate;
        }

        if($this->beforeSetDimensions !== null){
            $result['beforeSetDimensions'] = $this->beforeSetDimensions;
        }
        if($this->afterSetDimensions !== null){
            $result['afterSetDimensions'] = $this->afterSetDimensions;
        }

        if($this->beforeDataLimits !== null){
            $result['beforeDataLimits'] = $this->beforeDataLimits;
        }
        if($this->afterDataLimits !== null){
            $result['afterDataLimits'] = $this->afterDataLimits;
        }

        if($this->beforeBuildTicks !== null){
            $result['beforeBuildTicks'] = $this->beforeBuildTicks;
        }
        if($this->afterBuildTicks !== null){
            $result['afterBuildTicks'] = $this->afterBuildTicks;
        }

        if($this->beforeTickToLabelConversion !== null){
            $result['beforeTickToLabelConversion'] = $this->beforeTickToLabelConversion;
        }
        if($this->afterTickToLabelConversion !== null){
            $result['afterTickToLabelConversion'] = $this->afterTickToLabelConversion;
        }

        if($this->beforeCalculateTickRotation !== null){
            $result['beforeCalculateTickRotation'] = $this->beforeCalculateTickRotation;
        }
        if($this->afterCalculateTickRotation !== null){
            $result['afterCalculateTickRotation'] = $this->afterCalculateTickRotation;
        }

        if($this->beforeFit !== null){
            $result['beforeFit'] = $this->beforeFit;
        }

        if($this->afterFit !== null){
            $result['afterFit'] = $this->afterFit;
        }
        return $result;
    }


    /**
     * @return ScaleTime|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getTime(){
        if ($this->_time === null) {
            $this->setTime([]);
        }

        return $this->_time;
    }

    /**
     * @param ScaleTime|array $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setTime($value){
        if (is_array($value)) {
            $config = ['class' => ScaleTime::class];
            $this->_time = Yii::createObject(array_merge($config, $value));
        } elseif ($value instanceof ScaleTime) {
            $this->_time = $value;
        } else {
            throw new InvalidArgumentException('Only Time instance, configuration array or false is allowed.');
        }
    }
}