<?php
namespace kiozk\chart;

use yii\base\BaseObject;
use yii\base\InvalidArgumentException;

/**
 * Class ScaleTime
 * @package admin\chart
 *
 * @property string|null $unit
 * @property string|null $minUnit
 */
class ScaleTime extends BaseObject{
    const TIME_UNITS = [
        'millisecond',
        'second',
        'minute',
        'hour',
        'day',
        'week',
        'month',
        'quarter',
        'year'
    ];

    private $_unit;

    private $_minUnit;

    public $tooltipFormat;


    public function setUnit($value){
        if($value === false){
            $this->_unit = $value;
        } elseif(is_string($value)){
            if(in_array($value, static::TIME_UNITS)){
                $this->_unit = $value;
            } else {
                throw new InvalidArgumentException('Incorrect time unit value');
            }
        } else {
            throw new InvalidArgumentException('Incorrect time unit value');
        }
    }

    public function getUnit(){
        return $this->_unit;
    }

    public function setMinUnit($value){
        if($value === false){
            $this->_minUnit = $value;
        } elseif(is_string($value)){
            if(in_array($value, static::TIME_UNITS)){
                $this->_minUnit = $value;
            } else {
                throw new InvalidArgumentException('Incorrect time unit value');
            }
        } else {
            throw new InvalidArgumentException('Incorrect time unit value');
        }
    }

    public function getMinUnit(){
        return $this->_minUnit;
    }

    /**
     * @return  null|array
     */
    public function prepare(){
        $result = [];
        if($this->_unit !== null){
            $result['unit'] = $this->_unit;
        }
        if($this->_minUnit !== null){
            $result['minUnit'] = $this->_minUnit;
        }
        if($this->tooltipFormat !== null){
            $result['tooltipFormat'] = $this->tooltipFormat;
        }

        return empty($result) ? null : $result;
    }
}