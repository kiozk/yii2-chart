<?php
namespace kiozk\chart;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use yii\base\BaseObject;

class DataSet extends BaseObject implements Countable, IteratorAggregate {
    public $data = [];

    /**
     * The label for the dataset which appears in the legend and tooltips
     *
     * @var string
     */
    public $label;

    /**
     * @var
     */
    public $key;

    /**
     * @var string|string[]
     */
    public $backgroundColor;

    /**
     * @var string|string[]
     */
    public $borderColor;

    /**
     * @var int|int[]
     */
    public $borderWidth;

    /**
     * @var string|string[]
     */
    public $hoverBackgroundColor;

    /**
     * @var string|string[]
     */
    public $hoverBorderColor;

    /**
     * @var int|int[]
     */
    public $hoverBorderWidth;

    /**
     * @var int|int[]|null
     */
    public $hoverRadius;

    /**
     * @var string|null
     */
    public $stack;

    /**
     * @var int|null
     */
    public $pointRadius;

    /**
     * @var int|null
     */
    public $pointHoverRadius;
    /**
     * @var int|null
     */
    public $pointHitRadius;

    /**
     * @var string|null
     */
    public $type;
    /**
     * @var bool|null
     */
    public $fill;

    public function prepare(){
        $result = [
            'data' => array_values($this->data)
        ];

        if($this->label !== null) {
            $result['label'] = (string)$this->label;
        }
        if($this->backgroundColor !== null) {
            $result['backgroundColor'] = $this->backgroundColor;
        }
        if($this->borderColor !== null) {
            $result['borderColor'] = $this->borderColor;
        }
        if($this->borderWidth !== null) {
            $result['borderWidth'] = $this->borderWidth;
        }
        if($this->hoverBackgroundColor !== null) {
            $result['hoverBackgroundColor'] = $this->hoverBackgroundColor;
        }
        if($this->hoverBorderColor !== null) {
            $result['hoverBorderColor'] = $this->hoverBorderColor;
        }
        if($this->hoverBorderWidth !== null) {
            $result['hoverBorderWidth'] = $this->hoverBorderWidth;
        }
        if($this->hoverRadius !== null) {
            $result['hoverRadius'] = $this->hoverRadius;
        }
        if($this->stack !== null) {
            $result['stack'] = $this->stack;
        }
        if($this->pointRadius !== null) {
            $result['pointRadius'] = $this->pointRadius;
        }
        if($this->pointHoverRadius !== null) {
            $result['pointHoverRadius'] = $this->pointHoverRadius;
        }

        if($this->pointHitRadius !== null) {
            $result['pointHitRadius'] = $this->pointHitRadius;
        }
        if($this->type !== null) {
            $result['type'] = $this->type;
        }
        if($this->fill !== null) {
            $result['fill'] = $this->fill;
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * @inheritdoc
     */
    public function getIterator(){
        return new ArrayIterator($this->data);
    }
}