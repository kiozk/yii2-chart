<?php
namespace kiozk\chart;

use yii\base\BaseObject;

class Ticks extends BaseObject {
    /**
     * If true, automatically calculates how many labels that can be shown and hides labels accordingly. Turn it off to show all labels no matter what
     *
     * Default: true
     * @var bool|null
     */
    public $autoSkip;

    /**
     * Padding between the ticks on the horizontal axis when autoSkip is enabled. Note: Only applicable to horizontal scales.
     *
     * Default: 0
     * @var int
     */
    public $autoSkipPadding;

    /**
     * Returns the string representation of the tick value as it should be displayed on the chart.
     *
     * Js callback
     *
     * @var string
     */
    public $callback;

    /**
     * If true, show the ticks.
     *
     * @var bool
     *
     * Default: true
     */
    public $display;

    /**
     * Font color for the tick labels.
     * Default: #666666
     *
     * @var string
     */
    public $fontColor;

    /**
     * Font size for the tick labels.
     *
     * @var int
     */
    public $fontSize;

    /**
     * Font style for the tick labels, follows CSS font-style options (i.e. normal, italic, oblique, initial, inherit).
     *
     * @var string
     */
    public $fontStyle;

    /**
     * Distance in pixels to offset the label from the centre point of the tick (in the y direction for the x axis, and the x direction for the y axis).
     * Note: this can cause labels at the edges to be cropped by the edge of the canvas
     *
     * Default: 0
     *
     * @var int
     */
    public $labelOffset;

    /**
     * Maximum rotation for tick labels when rotating to condense labels. Note: Rotation doesn't occur until necessary.
     * Note: Only applicable to horizontal scales.
     *
     * Default: 90
     *
     * @var int
     */
    public $maxRotation;

    /**
     * Minimum rotation for tick labels.
     * Note: Only applicable to horizontal scales.
     *
     * Default: 0
     *
     * @var int
     */
    public $minRotation;

    /**
     * Flips tick labels around axis, displaying the labels inside the chart instead of outside.
     * Note: Only applicable to vertical scales.
     *
     * Default: false
     *
     * @var bool
     */
    public $mirror;

    /**
     * Padding between the tick label and the axis.
     * Note: Only applicable to horizontal scales.
     *
     * Default: 10
     *
     * @var int
     */
    public $padding;

    /**
     * Reverses order of tick labels.
     *
     * Default: false
     *
     * @var bool
     */
    public $reverse;

    /**
     * @var bool if true, scale will include 0 if it is not already included.
     * Default: false
     * @var bool
     */
    public $beginAtZero;

    /**
     * User defined minimum number for the scale, overrides minimum value from data.
     *
     * @var int
     */
    public $min;

    /**
     * User defined maximum number for the scale, overrides maximum value from data.
     *
     * @var int
     */
    public $max;

    /**
     * Maximum number of ticks and gridlines to show. If not defined, it will limit to 11 ticks but will show all gridlines.
     *
     * @var int
     */
    public $maxTicksLimit;

    /**
     * User defined fixed step size for the scale.
     * If set, the scale ticks will be enumerated by multiple of stepSize, having one tick per increment.
     * If not set, the ticks are labeled automatically using the nice numbers algorithm.
     *
     * @var int
     */
    public $fixedStepSize;

    /**
     * if defined, it can be used along with the min and the max to give a custom number of steps.
     *
     * @var int
     */
    public $stepSize;

    /**
     * User defined maximum number for the scale, overrides maximum value except for if it is lower than the maximum value.
     *
     * @var int
     */
    public $suggestedMax;

    /**
     * User defined minimum number for the scale, overrides minimum value except for if it is higher than the minimum value.
     *
     * @var int
     */
    public $suggestedMin;


    /**
     *
     * @return  null|array
     */
    public function prepare(){
        $result = [];

        if($this->autoSkip !== null) {
            $result['autoSkip'] = (bool) $this->autoSkip;
        }

        if($this->autoSkipPadding !== null) {
            $result['autoSkipPadding'] = (int) $this->autoSkip;
        }

        if($this->callback !== null) {
            $result['callback'] = $this->callback;
        }

        if($this->display !== null) {
            $result['display'] = (bool)$this->display;
        }

        if($this->fontColor !== null) {
            $result['fontColor'] = $this->fontColor;
        }

        if($this->fontSize !== null) {
            $result['fontSize'] = (int)$this->fontSize;
        }

        if($this->fontStyle !== null) {
            $result['fontStyle'] = $this->fontStyle;
        }

        if($this->labelOffset !== null) {
            $result['labelOffset'] = (int)$this->labelOffset;
        }

        if($this->maxRotation !== null) {
            $result['maxRotation'] = (int)$this->maxRotation;
        }

        if($this->minRotation !== null) {
            $result['minRotation'] = (int)$this->minRotation;
        }

        if($this->mirror !== null) {
            $result['mirror'] = (bool)$this->mirror;
        }

        if($this->padding !== null) {
            $result['padding'] = (int)$this->padding;
        }

        if($this->reverse !== null) {
            $result['reverse'] = (bool)$this->reverse;
        }
        if($this->beginAtZero !== null) {
            $result['beginAtZero'] = (bool)$this->beginAtZero;
        }

        if($this->min !== null) {
            $result['min'] = $this->min;
        }

        if($this->max !== null) {
            $result['max'] = $this->max;
        }

        if($this->maxTicksLimit !== null) {
            $result['maxTicksLimit'] = (int)$this->maxTicksLimit;
        }

        if($this->fixedStepSize !== null) {
            $result['fixedStepSize'] = (int)$this->fixedStepSize;
        }

        if($this->stepSize !== null) {
            $result['stepSize'] = (int)$this->stepSize;
        }

        if($this->suggestedMax !== null) {
            $result['suggestedMax'] = (int)$this->suggestedMax;
        }

        if($this->suggestedMin !== null) {
            $result['suggestedMin'] = (int)$this->suggestedMin;
        }
//var_dump($result);
        return empty($result) ?  null : $result;
    }
}