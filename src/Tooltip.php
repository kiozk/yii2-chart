<?php
namespace kiozk\chart;

use Yii;
use yii\base\InvalidArgumentException;
use yii\base\BaseObject;

class Tooltip extends BaseObject{
    /**
     * Finds all of the items that intersect the point
     */
    const MODE_POINT    = 'point';

    /**
     * Gets the item that is nearest to the point.
     * The nearest item is determined based on the distance to the center of the chart item (point, bar).
     * If 2 or more items are at the same distance, the one with the smallest area is used.
     * If intersect is true, this is only triggered when the mouse position intersects an item in the graph.
     * This is very useful for combo charts where points are hidden behind bars.
     */
    const MODE_NEAREST  = 'nearest';

    /**
     * Finds item at the same index.
     * If the intersect setting is true, the first intersecting item is used to determine the index in the data.
     * If intersect false the nearest item is used to determine the index.
     */
    const MODE_INDEX    = 'index';

    /**
     * Finds items in the same dataset.
     * If the intersect setting is true, the first intersecting item is used to determine the index in the data.
     * If intersect false the nearest item is used to determine the index.
     */
    const MODE_DATASET  = 'dataset';

    /**
     * Returns all items that would intersect based on the X coordinate of the position only.
     * Would be useful for a vertical cursor implementation. Note that this only applies to cartesian charts
     */
    const MODE_X        = 'x';

    /**
     * Returns all items that would intersect based on the Y coordinate of the position.
     * This would be useful for a horizontal cursor implementation. Note that this only applies to cartesian charts.
     */
    const MODE_Y        = 'y';

    /**
     * @var string
     */
    public $mode;

    /**
     * The tooltip label configuration is nested below the tooltip configuration using the callbacks key. The tooltip has the following callbacks for providing text. For all functions, 'this' will be the tooltip object created from the Chart.Tooltip constructor.
     *
     * @var TooltipCallbacks
     */
    private $_callbacks;

    public $intersect;
    /**
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     */
    public function prepare(){

        $result = [];
        $callbacks      = $this->getCallbacks();

        if(null !== $prepared = $callbacks->prepare()){
            $result['callbacks'] = $prepared;
        }

        if($this->mode !== null){
            $result['mode'] = $this->mode;
        }

        if($this->intersect !== null){
            $result['intersect'] = (bool)$this->intersect;
        }
        return empty($result) ? null : $result;
    }

    /**
     * @return TooltipCallbacks|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getCallbacks(){
        if ($this->_callbacks === null) {
            $this->setCallbacks([]);
        }

        return $this->_callbacks;
    }

    /**
     * @param TooltipCallbacks|array $value
     * @throws \yii\base\InvalidConfigException
     */
    public function setCallbacks($value){
        if (is_array($value)) {
            $this->_callbacks = Yii::createObject(array_merge(['class' => TooltipCallbacks::class], $value));
        } elseif ($value instanceof Ticks) {
            $this->_callbacks = $value;
        } else {
            throw new InvalidArgumentException('Only TooltipCallbacks instance, configuration array or false is allowed.');
        }
    }
}