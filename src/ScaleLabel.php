<?php
namespace kiozk\chart;

use yii\base\BaseObject;

class ScaleLabel extends BaseObject {
    /**
     * @var bool
     */
    public $display;

    /**
     * The text for the title. (i.e. "# of People", "Response Choices")
     *
     * Default: ''
     *
     * @var string
     */

    public $labelString;

    /**
     * Font color for the scale title.
     *
     * Default: #666666
     *
     * @var string
     */
    public $fontColor;

    /**
     * Font family for the scale title, follows CSS font-family options.
     *
     * Default: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"
     * @var string
     */
    public $fontFamily;

    /**
     * Font size for the scale title.
     *
     * @var int
     */
    public $fontSize;


    /**
     * Font style for the scale title, follows CSS font-style options (i.e. normal, italic, oblique, initial, inherit).
     *
     * @var string
     */
    public $fontStyle;

    /**
     * @return  null|array
     */
    public function prepare(){
        $result = [];
        if($this->display !== null){
            $result['display'] = (bool)$this->display;
        }
        if($this->labelString !== null){
            $result['labelString'] = $this->labelString;
        }
        if($this->fontColor !== null){
            $result['fontColor'] = $this->fontColor;
        }
        if($this->fontFamily !== null){
            $result['fontFamily'] = $this->fontFamily;
        }
        if($this->fontSize !== null){
            $result['fontSize'] = $this->fontSize;
        }
        if($this->fontStyle !== null){
            $result['fontStyle'] = $this->fontStyle;
        }
        return empty($result) ? null : $result;
    }
}