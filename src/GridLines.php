<?php
namespace kiozk\chart;

class GridLines {

    /**
     * Default: true
     *
     * @var bool|null
     */
    public $display;

    /**
     * Color of the grid lines.
     *
     * Default: "rgba(0, 0, 0, 0.1)"
     *
     * @var string|string[]|null
     */
    public $color;

    /**
     * Length and spacing of dashes.
     * @see https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setLineDash
     *
     * Default: []
     *
     * @var int[]
     */
    public $borderDash;

    /**
     * Offset for line dashes.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineDashOffset
     *
     * Default: 0.0
     *
     * @var float
     */
    public $borderDashOffset;

    /**
     * Stroke width of grid lines
     *
     * Default: 1
     *
     * @var int|int[]|null
     */
    public $lineWidth;

    /**
     * If true draw border on the edge of the chart
     *
     * Default: true
     *
     * @var bool|null
     */
    public $drawBorder;

    /**
     * If true, draw lines on the chart area inside the axis lines. This is useful when there are multiple axes and you need to control which grid lines are drawn
     *
     * Default: true
     *
     * @var bool|null
     */
    public $drawOnChartArea;

    /**
     * If true, draw lines beside the ticks in the axis area beside the chart.
     *
     * Default: true
     *
     * @var bool|null
     */
    public $drawTicks;

    /**
     * Length in pixels that the grid lines will draw into the axis area.
     *
     * Default: 10
     *
     * @var int|null
     */
    public $tickMarkLength;

    /**
     * Stroke width of the grid line for the first index (index 0).
     *
     * Default: 1
     *
     * @var int|null
     */
    public $zeroLineWidth;

    /**
     * Stroke color of the grid line for the first index (index 0).
     *
     * Default: "rgba(0, 0, 0, 0.25)"
     *
     * @var string|null
     */
    public $zeroLineColor;

    /**
     * If true, labels are shifted to be between grid lines. This is used in the bar chart.
     *
     * Default: false
     *
     * @var bool|null
     */
    public $offsetGridLines;

    /**
     * @return null|array
     */
    public function prepare(){
        $result = [];
        if($this->display !== null){
            $result['display'] = (bool)$this->display;
        }
        if($this->color !== null){
            $result['color'] = $this->color;
        }
        if($this->borderDash !== null){
            $result['borderDash'] = $this->borderDash;
        }
        if($this->borderDashOffset !== null){
            $result['borderDashOffset'] = (float)$this->borderDashOffset;
        }
        if($this->lineWidth !== null){
            $result['lineWidth'] = $this->lineWidth;
        }
        if($this->drawBorder !== null){
            $result['drawBorder'] = (bool)$this->drawBorder;
        }
        if($this->drawOnChartArea !== null){
            $result['drawOnChartArea'] = (bool)$this->drawOnChartArea;
        }
        if($this->drawTicks !== null){
            $result['drawTicks'] = (bool)$this->drawTicks;
        }
        if($this->tickMarkLength !== null){
            $result['tickMarkLength'] = (int)$this->tickMarkLength;
        }
        if($this->zeroLineWidth !== null){
            $result['zeroLineWidth'] = (int)$this->zeroLineWidth;
        }
        if($this->zeroLineColor !== null){
            $result['zeroLineColor'] = $this->zeroLineColor;
        }
        if($this->offsetGridLines !== null){
            $result['offsetGridLines'] = (bool)$this->offsetGridLines;
        }
        return empty($result) ? null : $result;
    }
}