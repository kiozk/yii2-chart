+function ($) {
    'use strict';
    Chart.Ticks.formatters.percentage = function(value){
        return (Math.round(value * 10000) / 100).toString() + '%';

    };

    Chart.Ticks.formatters.dateShort = function(value, index){

        return moment(value, 'YYYY-MM-DD').format('D MMM');

    };

    Chart.Ticks.formatters.timestampForHourly = function(value, index){
        return moment(value, 'X').format('dddd, DD MMMM YYYY HH:00-HH:59');
    };

    Chart.Ticks.formatters.timestampForDateShort = function(value, index){
        return moment(value, 'X').format('DD MMM');
    };

    Chart.Ticks.formatters.size = function(value, si){
        var thresh = si ? 1000 : 1024;
        if(Math.abs(value) < thresh) {
            return value + ' B';
        }
        var units = si
            ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
            : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
        var u = -1;
        do {
            value /= thresh;
            ++u;
        } while(Math.abs(value) >= thresh && u < units.length - 1);
        return value.toFixed(1)+' '+units[u];

    };
    Chart.Ticks.formatters.dateFull = function(value){
       return moment(value, 'YYYY-MM-DD').format('dddd, DD MMMM YYYY');
    };

    Chart.Ticks.formatters.reduction = function(value){

        if(value > 999999999) {
            return Math.ceil(value / 1000000000).toString() + 'g'
        }else if(value > 999999) {
            return Math.ceil(value / 1000000).toString() + 'm'
        }else if(value > 999) {
            return Math.ceil(value / 1000).toString() + 'k'
        } else {
            return Math.floor(value * 1000000) / 1000000;
        }

    };

    Chart.Ticks.formatters.timeInMinutes = function(value){

        value = Math.ceil(value);
        var  minutes = Math.ceil(value / 60), seconds = (value % 60);
        if(seconds < 10){
            seconds = '0' + seconds.toString();
        }

        return  minutes + ':' + seconds;

    };
    var PercentageLinearScale = Chart.scaleService.getScaleConstructor('linear').extend({
        getLabelForIndex: function(index, datasetIndex) {
            var me = this;
            var data = me.chart.data;
            var val = me.getRightValue(data.datasets[datasetIndex].data[index]);

            return Chart.Ticks.formatters.percentage(val);
        }
    });

    Chart.scaleService.registerScaleType('percentage', PercentageLinearScale, {
        ticks: {
            position: 'right',
            callback: Chart.Ticks.formatters.percentage
        }
    });

    var StringDateLinearScale = Chart.scaleService.getScaleConstructor('category').extend({
        getLabelForIndex: function(index, datasetIndex) {
            var me = this, labels = me.chart.data.labels, val = labels[index], isHorizontal = me.isHorizontal();
            return Chart.Ticks.formatters.dateFull(val);
        }
    });

    Chart.scaleService.registerScaleType('stringDate', StringDateLinearScale, {
        position: 'bottom',
        ticks: {
            callback: Chart.Ticks.formatters.dateShort,
            maxRotation: 0,
            autoSkip: true,
            autoSkipPadding: 50
        },
        categoryPercentage: 0.8,
        barPercentage: 0.9,

        // grid line settings
        gridLines: {
            offsetGridLines: true
        }
    });

    var LinearWithProportionScale = Chart.scaleService.getScaleConstructor('linear').extend({
        getLabelForIndex: function(index, datasetIndex) {
            var me = this, data = me.chart.data, val = me.getRightValue(data.datasets[datasetIndex].data[index]), sum = 0, percentage;
            $.each(data.datasets, function () {
                sum +=  this.data[index];
            });
            if(sum > 0){
                percentage = val / sum;
            } else {
                percentage = 0.0;
            }
            return val + ' (' + Chart.Ticks.formatters.percentage(percentage) + ')';
        }
    });


    Chart.scaleService.registerScaleType('linearWithProportion', LinearWithProportionScale, {
        ticks: {
            position: 'left',
            callback: Chart.Ticks.formatters.reduction
        }
    });

    var ReductionLinearScale = Chart.scaleService.getScaleConstructor('linear').extend({

    });

    Chart.scaleService.registerScaleType('reductionLinear', ReductionLinearScale, {
        ticks: {
            position: 'left',
            callback: Chart.Ticks.formatters.reduction
        }
    });
    var TimeLinearScale = Chart.scaleService.getScaleConstructor('linear').extend({
        getLabelForIndex: function(index, datasetIndex) {


            var me = this,  isHorizontal = me.isHorizontal(), val;
            if(isHorizontal){
                var labels = me.chart.data.labels;
                val = labels[index];

            } else {
                val = me.getRightValue(me.chart.data.datasets[datasetIndex].data[index])
            }
            console.dir(val);
            return Chart.Ticks.formatters.timeInMinutes(val);
        }
    });

    Chart.scaleService.registerScaleType('timeLinear', TimeLinearScale, {
        ticks: {
            position: 'left',
            callback: Chart.Ticks.formatters.timeInMinutes
        }
    });


    var TimestampLinearScale = Chart.scaleService.getScaleConstructor('category').extend({
        getLabelForIndex: function(index, datasetIndex) {
            var me = this, labels = me.chart.data.labels, val = labels[index], isHorizontal = me.isHorizontal();
            return Chart.Ticks.formatters.timestampForHourly(val);
        }
    });

    Chart.scaleService.registerScaleType('timestampHourLinear', TimestampLinearScale, {
        position: 'bottom',
        ticks: {
            callback: Chart.Ticks.formatters.timestampForDateShort,
            maxRotation: 0,
            autoSkip: true,
            autoSkipPadding: 50
        },
        categoryPercentage: 0.8,
        barPercentage: 0.9,

        // grid line settings
        gridLines: {
            offsetGridLines: true
        }
    });


    var LogarithmicExScale = Chart.scaleService.getScaleConstructor('logarithmic').extend({
        getLabelForIndex: function(index, datasetIndex) {
            var me = this, labels = me.chart.data.labels, val = labels[index], isHorizontal = me.isHorizontal();
            return Chart.Ticks.formatters.reduction(val);
        }
    });

    Chart.scaleService.registerScaleType('logarithmicEx', LogarithmicExScale, {
        position: 'bottom',
        ticks: {
            callback: Chart.Ticks.formatters.reduction,
          //  maxRotation: 0,
        //    autoSkip: true,
         //   autoSkipPadding: 50
        },
      //  categoryPercentage: 0.8,
      //  barPercentage: 0.9

        // grid line settings
        gridLines: {
            offsetGridLines: true
        }
    });
}(jQuery);